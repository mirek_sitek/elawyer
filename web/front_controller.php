<?php

//automatyczne ładowanie plików klas - automatycznie inkluduje potrzebne pliki podczas tworzenia instancji klasy
    spl_autoload_register(function($classname) {
//    echo __DIR__ . '<br>';
//    echo str_replace('\\', '/', $classname). '<br>';
    //echo $classname . '.php'. '<br>';
    require_once(__DIR__ . '/../' . str_replace('\\', '/', $classname) . '.php');
});

//Twig autoloader
require_once '../vendor/autoload.php';
Twig_Autoloader::register();

use core\App;


$app = new App(__DIR__);

//routing

$app->route('/users', 'UsersController', 'users');
$app->route('/view-user', 'UsersController', 'user');
$app->route('/edit-user', 'UsersController', 'edit');
$app->route('/new-user', 'UsersController', 'new_user');
$app->route('/login', 'UsersController', 'login');
$app->route('/logout', 'UsersController', 'logout');
$app->route('/delete', 'UsersController', 'delete_user');
$app->route('/deleted', 'UsersController', 'deleted');

$app->route('/lawyers', 'LawyersController', 'lawyers');
$app->route('/view-lawyer', 'LawyersController', 'lawyer');
$app->route('/edit-lawyer', 'LawyersController', 'edit');
$app->route('/', 'LawyersController', 'find_lawyer');
$app->route('/find', 'LawyersController', 'find_lawyer');
$app->route('/no-lawyer', 'LawyersController', 'no_lawyer');

//static contents
$app->route('/about', 'static', 'about_us');
$app->route('/prywatnosc', 'static', 'privacy');
$app->route('/regulamin', 'static', 'rules');
$app->route('/artykuly', 'static', 'SWP');
$app->route('/dokumenty', 'static', 'SWP');
$app->route('/adresy-urzedow', 'static', 'SWP');
$app->route('/akty-prawne', 'static', 'SWP');
$app->route('/wrong_user', 'static', 'wrong_user');
$app->route('/no-page', 'static', 'no_page');
$app->route('/user-added', 'static', 'user_added');
$app->route('/user-changed', 'static', 'user_changed');
$app->route('/lawyer-changed', 'static', 'lawyer_data_changed');
$app->route('/no-access', 'static', 'no_access');
$app->route('/jestes', 'static', 'jestes_prawnikiem');


//good to go

$app->run();


