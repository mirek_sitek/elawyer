<?php

namespace controllers;

use business\lawyersService;
use business\LawyerClass;
use business\UserClass;
use core\App;

class LawyersController {

    use ControllerUtils;

    private $lawyersService;

    public function __construct() {
        $this->lawyersService = new lawyersService();
    }


    function lawyers(&$model) { // &przez referencje czyli działamy na modelu
        if ($_SESSION['role'] === 'admin') {
            $lawyers = $this->lawyersService->get_lawyers(); //pytanie do bazy danych o lawyerów

            $model['lawyers'] = $lawyers; // w modelu są teraz lawyerzy

            return 'lawyers_view';
        } //zwraca nazwę widoku, który jest przypisany do metody
        else {
            return 'redirect:no-access';
        }
    }


    function lawyer(&$model) {
        if (!empty($_GET['id'])) {
            $id = $_GET['id'];
            if ($lawyer = $this->lawyersService->get_lawyer_by_Id($id)) {
                $model['lawyer'] = $lawyer;
                if (isset($_SESSION['role'])) {
                    return 'my_data_view';
                } else {
                    return 'lawyer_view';
                }
            }
        }

        http_response_code(404);
        exit;
    }

    function edit(&$model) {
        if ($_SESSION['role'] === 'admin' or $_SESSION['id_lawyer'] == $_GET['id']) {
            if (!isset($model->name)) {
                $lawyer = new LawyerClass();
                $lawyer->id_user = new UserClass();
            } else {
                $lawyer = $model;
            }
            if ($this->is_post()) {
                if (!empty($_POST['name']) &&
                        !empty($_POST['login']) &&
                        !empty($_POST['pass']) &&
                        !empty($_POST['city']) &&
                        !empty($_POST['email']) &&
                        !empty($_POST['role']) &&
                        !empty($_POST['NIP']) &&
                        !empty($_POST['street_addr']) &&
                        !empty($_POST['phone']) &&
                        !empty($_POST['accnt']) &&
                        !empty($_POST['spec'])
                ) {
                    if (isset($_POST['id'])) {
                        $lawyer->Id = $_POST['id'];
                    }

                    $lawyer->id_user->Id = $_POST['id_user'];
                    $lawyer->id_user->name = $_POST['name'];
                    if ($_SESSION['role'] != 'admin') {
                        $_SESSION['name'] = $lawyer->id_user->name;
                    }
                    $lawyer->id_user->last_name = $_POST['last_name'];
                    $lawyer->id_user->login = $_POST['login'];
                    $lawyer->id_user->pass = $_POST['pass'];
                    $lawyer->id_user->city = $_POST['city'];
                    $lawyer->id_user->email = $_POST['email'];
                    $lawyer->id_user->role = $_POST['role'];
                    $lawyer->nip = $_POST['NIP'];
                    $lawyer->street_addr = $_POST['street_addr'];
                    $lawyer->phone = $_POST['phone'];
                    $lawyer->accnt = $_POST['accnt'];
                    $lawyer->spec = $_POST['spec'];

                    if ($this->lawyersService->save_lawyer($lawyer)) {
                        return 'redirect:lawyer-changed';
                    }
                }
            } elseif (!empty($_GET['id'])) {

                $lawyer = $this->lawyersService->get_lawyer_by_Id($_GET['id']);
                if (!isset($lawyer->Id)) {
                    if ($_GET['id'] != '-1') {

                        return 'redirect:no-lawyer';
                    } else {
                        $model['lawyer'] = $lawyer;
                        return 'lawyer_new_view';
                    }
                }
            }

            $model['lawyer'] = $lawyer;

            return 'lawyer_edit_view';
        } else {
            return 'redirect:no-access';
        }
    }


    function index(&$model) {
        return 'index';
    }


    function find_lawyer(&$model) {

        $lawyer = [];

        if ($this->is_post()) {
            $lawyer = new LawyerClass();
            $lawyer->id_user = new UserClass();
            $city = $_POST['city'];
            $spec = $_POST['spec'];
            if ($lawyer = $this->lawyersService->get_lawyers_by_city_spec($city, $spec)) {
                $model['lawyers'] = $lawyer;
                return "redirect: find?spec=" . $spec . "&city=" . $city;
            } else {
                return "redirect: find?spec=" . $spec . "&city=" . $city;
            }

        } else {
            if (!empty($_GET['spec']) ||
                    !empty($_GET['city'])
            ) {
                $city = $_GET['city'];
                $spec = $_GET['spec'];
                $model['param'] = [$spec, $city];
                if ($lawyer = $this->lawyersService->get_lawyers_by_city_spec($city, $spec)) {
                    $model['lawyers'] = $lawyer;
                    return 'find_lawyer';
                } else {
                    $model['lawyers'] = ['pusto'];
                    return 'find_lawyer';
                }
            }
        }


        return 'find_lawyer';
    }

    function logout() {
        $_SESSION['id'] = null;
        $_SESSION['name'] = null;
        session_destroy();
        return 'redirect:users';
    }

    function delete_user(&$model) {
        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];

            if ($this->is_post()) {
                $this->lawyersService->delete_user($id);
                return 'redirect:deleted';
            } else {
                if ($user = $this->lawyersService->get_user_by_Id($id)) {
                    $model = $user;
                    return 'delete_view';
                }
            }
        }

        http_response_code(404);
        exit;
    }

    function deleted() {
        return 'deleted_view';
    }

    function no_lawyer() {
        return 'no_lawyer';
    }

}