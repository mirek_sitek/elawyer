<?php

namespace controllers;

use business\UsersService;
use business\lawyersService;
use business\UserClass;
use core\App;

class UsersController {

    use ControllerUtils;

    private $usersService;
    private $lawyersService;

    public function __construct() {
        $this->usersService = new UsersService();
        $this->lawyersService = new lawyersService();
    }

    function users(&$model) { // &przez referencje czyli działamy na modelu
        if ($_SESSION['role'] === 'admin') {
            $users = $this->usersService->get_users(); //pytanie do bazy danych o userów

            $model['users'] = $users; // w modelu są teraz userzy
            return 'users_view';
        }//zwraca nazwę widoku, który jest przypisany do metody
        else {
            return 'redirect:no-access';
        }
    }

    function user(&$model) {
        if (!empty($_GET['id'])) {
            $id = $_GET['id'];

            if ($users = $this->usersService->get_user_by_Id($id)) {
                $model['user'] = $users;

                return 'user_view';
            }
        }

        http_response_code(404);
        exit;
    }

    function edit(&$model) {
        if ($_SESSION['role'] === 'admin' or $_SESSION['id'] == $_GET['id']) {
            $user = new UserClass();

            if ($this->is_post()) {
                if (!empty($_POST['name']) &&
                        !empty($_POST['login']) &&
                        !empty($_POST['pass']) &&
                        !empty($_POST['city']) &&
                        !empty($_POST['email']) &&
                        !empty($_POST['role'])
                ) {
                    $user->Id = isset($_POST['id']) ? $_POST['id'] : null;

                    $user->name = $_POST['name'];
                    if ($_SESSION['role'] != 'admin') {
                        $_SESSION['name'] = $user->name;
                    }
                    $user->last_name = $_POST['last_name'];
                    $user->login = $_POST['login'];
                    $user->pass = $_POST['pass'];
                    $user->city = $_POST['city'];
                    $user->email = $_POST['email'];
                    $user->role = $_POST['role'];
                    if ($this->usersService->save_user($user)) {
                        return 'redirect:user-changed';
                    }
                }
            } elseif (!empty($_GET['id'])) {
                $user = $this->usersService->get_user_by_Id($_GET['id']);
            }

            $model['user'] = $user;

            return 'user_edit_view';
        } else {
            return 'redirect:no-access';
        }
    }

    function new_user(&$model) {

        $user = new UserClass();

        if ($this->is_post()) {     //jeśli POST (czyli submit)
            if (!empty($_POST['name']) && //jeśli pola nie są puste
                    !empty($_POST['login']) &&
                    !empty($_POST['pass']) &&
                    !empty($_POST['city']) &&
                    !empty($_POST['email']) &&
                    !empty($_POST['role'])
            ) {
                if (isset($_POST['id'])) {
                    $user->Id = $_POST['id'];
                }

                $user->name = $_POST['name'];
                $user->last_name = $_POST['last_name'];
                $user->login = $_POST['login'];
                $user->pass = $_POST['pass'];
                $user->city = $_POST['city'];
                $user->email = $_POST['email'];
                $user->role = $_POST['role'];
                if (empty($this->usersService->get_user_by_login($user->login)->login)) {
                    if ($this->usersService->save_user($user)) {
                        return 'redirect:user-added';
                    }
                } else {
                    return 'wrong_user';
                }
            }
        } elseif (!empty($_GET['id'])) {
            $user = $this->usersService->get_user_by_Id($_GET['id']);
        }

        $model['user'] = $user;

        return 'user_new_view';
    }

    function index(&$model) {
        return 'index';
    }

    function login(&$model) {

        $user = new UserClass();
        $model = '';

        if ($this->is_post()) {
            if (!empty($_POST['login']) &&
                    !empty($_POST['pass'])
            ) {
                $login = $_POST['login'];
                if ($user = $this->usersService->get_user_by_login($login)) {
                    if (password_verify($_POST['pass'], $user->pass)) {
                        session_regenerate_id();
                        $_SESSION['id'] = $user->Id;
                        $_SESSION['name'] = $user->name;
                        $_SESSION['role'] = $user->role;
                        $_SESSION['id_lawyer'] = $this->lawyersService->get_lawyer_id_by_user_id($user->Id);
                        return 'redirect:find';
                    } else {
                        $model['badlogin'] = 'Nieprawidłowy login albo hasło';
                    }
                } else {
                    $model['badlogin'] = 'Nieprawidłowy login albo hasło';
                }
            }
        }
        return 'login';
    }

    function logout() {
        $_SESSION['id'] = null;
        $_SESSION['name'] = null;
        session_destroy();
        return 'redirect:find';
    }

    function delete_user(&$model) {
        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];

            if ($this->is_post()) {

                $this->usersService->delete_user($id);
                return 'redirect:deleted';
            } else {
                if ($user = $this->usersService->get_user_by_Id($id)) {
                    $model["user"] = $user;
                    return 'delete_view';
                }
            }
        }

        http_response_code(404);
        exit;
    }

    function deleted() {
        return 'deleted_view';
    }

    function wrong_user() {
        return 'wrong_user';
    }

}
