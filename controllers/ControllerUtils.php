<?php

namespace controllers;


trait ControllerUtils
{
    function is_post()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    function is_get()
    {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    function is_ajax()
    {
        return
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'XMLHttpRequest') === 0;
    }
}
