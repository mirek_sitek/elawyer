<?php

namespace business;

use PDO;
use business\UserClass;

define("DB_HOST", "localhost");
define("DB_USER", "mirek");
define("DB_PASS", "05Santoku*22");
define("DB_NAME", "elawyer");

class DBService {

    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
    //
    private $dbh; //Database Handler
    private $error;  //errors
    //
    private $stmt; //statement for queries
    //
    

    public function __construct() {
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }

        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    public function query($query) {
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute() {
        return $this->stmt->execute();
    }

    public function resultset($class) {
//        $this->stmt->setFetchMode(PDO::FETCH_CLASS, 'business\UserClass');        
        $this->stmt->setFetchMode(PDO::FETCH_CLASS, $class);        
        $this->execute();
        return $this->stmt->fetchAll();
    }

    //---- ważne musi być 2 razy FETCH_CLASS
    public function single($class) {
//        $this->stmt->setFetchMode(PDO::FETCH_CLASS, 'business\UserClass');
        $this->stmt->setFetchMode(PDO::FETCH_CLASS, $class);
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_CLASS);
    }

    public function rowCount() {
        $this->execute();
        return $this->stmt->rowCount();
    }

    public function lastInsertId() {
        return $this->dbh->lastInsertId();
    }

    public function debugDumpParams() {
        return $this->stmt->debugDumpParams();
    }

}
