<?php

namespace business;

class UsersService {

    private $db;
    private $usr;

    public function __construct() {
        $this->db = new DBService();
        $this->usr = new UserClass();
    }

    function get_users() {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users");
        return $this->db->resultset('business\UserClass'); // zwracamy wszystkich użytkowników z bazy
    }

    function get_users_by_city($city) {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users WHERE city = :b_city");
        $this->db->bind(':b_city', $city);
        return $this->db->resultset('business\UserClass'); // zwracamy użytkowników z bazy z miasta
    }

    function get_user_by_Id($id) {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users WHERE Id = :b_Id");
        $this->db->bind(':b_Id', $id);
        return $this->db->single('business\UserClass'); // zwracamy jednego użytkownika o Id
    }

    function get_user_by_login($login) {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users WHERE login = :b_login");
        $this->db->bind(':b_login', $login);
        return $this->db->single('business\UserClass'); // zwracamy jednego użytkownika o name
    }

    function save_user($user) {
        echo 'W set_user';

        // zapis kiedy Id == null
        if (($user->Id) == NULL) {
            $this->db->query("INSERT INTO users (name, last_name, login, pass, email, role, city) VALUES (:b_name, :b_last_name, :b_login, :b_pass, :b_email, :b_role, :b_city)");
            foreach ($user as $key => $value) {
                if ($key != 'Id') {
                    $this->db->bind(':b_' . $key, $value);
                }
            }
            $this->db->bind(':b_pass', password_hash($user->pass, PASSWORD_DEFAULT)); //hash hasła
            $this->db->execute();
        }

        //update kiedy Id != null
        else {
            $this->db->query("UPDATE users SET name = :b_name, last_name = :b_last_name, city = :b_city, email = :b_email, role = :b_role WHERE Id = :b_Id");
            $this->db->bind(':b_name', $user->name);
            $this->db->bind(':b_last_name', $user->last_name);
            $this->db->bind(':b_city', $user->city);
            $this->db->bind(':b_email', $user->email);
            $this->db->bind(':b_Id', $user->Id);
            $this->db->bind(':b_role', $user->role);
            $this->db->execute();
        }

        echo 'Inserted';
        return true;
    }

    function update_user($user) {
        echo 'W update_user';
        $this->db->query("UPDATE users SET login = b:_login, pass = b:_pass, name = b:_name, last_name = b:last_name, city = b:_city, email = b:_email, role = b:_role WHERE Id = b:_Id");
       //                   UPDATE users SET name = :b_name, last_name = :b_last_name, city = :b_city, email = :b_email, role = :b_role WHERE Id = :b_Id
        foreach ($user as $key => $value) {
            $this->db->bind(':b_' . $key, $value);
        }
        $this->db->bind(':b_pass', password_hash($user->pass, PASSWORD_DEFAULT)); //hash hasła
        $this->db->execute();

        echo 'Inserted';
        return true;
    }

    function delete_user($id) {
        $this->db->query("DELETE FROM users WHERE Id = :b_Id");
        $this->db->bind(':b_Id', $id);
        $this->db->execute();
    }

    function count_users() {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users");

        return $this->db->rowCount();
    }
    
    function last_inserted(){
        return $this->db->lastInsertId();
    }

}
