<?php

namespace business;

use business\UsersService;

class lawyersService {

    private $db;
    private $userService;
    private $lawyer;
    private $user;
    private $lastInserted;
    private $temp;

    public function __construct() {
        $this->db = new DBService();
        $this->lawyer = new LawyerClass();
        $this->user = new UserClass();
        $this->userService = new UsersService();
    }

    function get_lawyers() {
        $this->db->query("SELECT * FROM lawyers");
        $this->lawyer = $this->db->resultset('business\LawyerClass'); // wszyscy prawnicy z bazy
        $this->user = $this->userService->get_users(); //wszyscy użytkownicy;

        foreach ($this->lawyer as $lawer) {
            foreach ($this->user as $user) {
                if ($lawer->id_user == $user->Id) {
                    $lawer->id_user = $user;
                }
            }
        }
        return $this->lawyer; // zwracamy prawników 
    }

    function get_lawyers_by_city($city) {
        $this->db->query("SELECT lawyers.Id, id_user, nip, street_addr, phone, accnt, spec, rating FROM lawyers INNER JOIN users ON lawyers.id_user=users.Id WHERE city = :b_city");
        $this->db->bind(':b_city', $city);
        $this->lawyer = $this->db->resultset('business\LawyerClass');
        $this->user = $this->userService->get_users();

        foreach ($this->lawyer as $lawer) {
            foreach ($this->user as $user) {
                if ($lawer->id_user == $user->Id) {
                    $lawer->id_user = $user;
                }
            }
        }

        return $this->lawyer; // zwracamy prawników z bazy z miasta
    }

    function get_lawyers_by_city_spec($city, $spec) {
        $this->db->query("SELECT lawyers.Id, id_user, nip, street_addr, phone, accnt, spec, rating FROM lawyers INNER JOIN users ON lawyers.id_user=users.Id WHERE users.city LIKE COALESCE(:b_city,'%') AND lawyers.spec like COALESCE(:b_spec,'%')");
        $city == '' ? $this->db->bind(':b_city', NULL) : $this->db->bind(':b_city', '%' . $city . '%');
        $spec == '' ? $this->db->bind(':b_spec', NULL) : $this->db->bind(':b_spec', '%' . $spec . '%');
        $this->lawyer = $this->db->resultset('business\LawyerClass');
        $this->user = $this->userService->get_users();

        foreach ($this->lawyer as $lawer) {
            foreach ($this->user as $user) {
                if ($lawer->id_user == $user->Id) {
                    $lawer->id_user = $user;
                }
            }
        }

        return $this->lawyer; // zwracamy prawników z bazy (miasto, specjalizacja)
    }

    function get_lawyer_by_Id($id) {
        $this->db->query("SELECT * FROM lawyers WHERE Id= :b_Id");
        $this->db->bind(':b_Id', $id);
        $this->lawyer = $this->db->single('business\LawyerClass');
        if (!empty($this->lawyer)) {
            $this->user = $this->userService->get_user_by_Id($this->lawyer->id_user);
            $this->lawyer->id_user = $this->user;
            return $this->lawyer;
        } elseif (!empty($_SESSION['id'])) {               // zwracamy jednego prawnika o Id
            $this->lawyer = new LawyerClass();
            $this->user = $this->userService->get_user_by_Id($_SESSION['id']);
            $this->lawyer->id_user = $this->user;
            return $this->lawyer;
        } else {
            $this->lawyer = new LawyerClass();
            return $this->lawyer;
        }
    }

    function get_lawyer_by_login($login) {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users WHERE login = :b_login");
        $this->db->bind(':b_login', $login);
        return $this->db->single('business\LawyerClass'); // zwracamy jednego użytkownika o name
    }

    function get_lawyer_id_by_user_id($id) {
        $this->db->query("SELECT Id, id_user FROM lawyers WHERE id_user = :b_id_user");
        $this->db->bind(':b_id_user', $id);
        $this->temp = $this->db->single('business\LawyerClass')->Id; // zwracamy jednego użytkownika o name
        if (empty($this->temp)) {
            return '-1';
        } else {
            return $this->temp;
        }
    }

    function save_lawyer($lawyer) {
        if (($lawyer->Id) == NULL) {
            $this->db->query("INSERT INTO lawyers (id_user, nip, street_addr, phone, accnt, spec, rating) VALUES (:b_id_user, :b_nip, :b_street_addr, :b_phone, :b_accnt, :b_spec, :b_rating)");
            foreach ($lawyer as $key => $value) {
                if ($key != 'Id' && $key != 'id_user') {
                    $this->db->bind(':b_' . $key, $value);
                }
            }
            $this->db->bind(':b_id_user', $_SESSION['id']);
            $this->db->execute();
            $_SESSION['id_lawyer'] = $this->db->lastInsertId();
            $this->userService->save_user($lawyer->id_user); //update user role
            $_SESSION['role'] = 'lawyer'; // update sesion role
        }
        else {
            $this->userService->save_user($lawyer->id_user);
            $this->db->query("UPDATE lawyers SET nip = :b_nip, street_addr = :b_street_addr, phone = :b_phone, accnt = :b_accnt, spec = :b_spec  WHERE Id = :b_Id");
            $this->db->bind(':b_nip', $lawyer->nip);
            $this->db->bind(':b_street_addr', $lawyer->street_addr);
            $this->db->bind(':b_phone', $lawyer->phone);
            $this->db->bind(':b_accnt', $lawyer->accnt);
            $this->db->bind(':b_spec', $lawyer->spec);
            $this->db->bind(':b_Id', $lawyer->Id);
            $this->db->execute();
        }
        return true;
    }

    function update_lawyer($user) {
        echo 'W update_user';
        $this->db->query("UPDATE users SET login = b:_login, pass = b:_pass, name = b:_name, last_name = b:last_name, city = b:_city, email = b:_email WHERE Id = b:_Id");
        foreach ($user as $key => $value) {
            $this->db->bind(':b_' . $key, $value);
        }
        $this->db->bind(':b_pass', password_hash($user->pass, PASSWORD_DEFAULT)); //hash hasła
        $this->db->execute();
        return true;
    }

    function delete_lawyer($id) {
        $this->db->query("DELETE FROM users WHERE Id = :b_Id");
        $this->db->bind(':b_Id', $id);
        $this->db->execute();
    }

    function count_lawyer() {
        $this->db->query("SELECT Id, name, last_name, login, pass, email, role, city FROM users");
        return $this->db->rowCount();
    }

}
