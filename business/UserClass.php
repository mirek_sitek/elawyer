<?php

namespace business;

class UserClass {

    public $Id;
    public $name;
    public $last_name;
    public $login;
    public $pass;
    public $city;
    public $email;
    public $role;
    
    public function full_name() {
        echo ( $this->name . ' ' . $this->last_name);
    }

    public function tablica() {
        return ['imie' => $this->name, 'nazwisko' => $this->last_name, 'miasto' => $this->city];
    }

}
