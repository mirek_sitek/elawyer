# Eprawnik

* Eprawnik - projekt zaliczeniowy Aplikacje i Usługi Internetowe
* prowadzący: dr Tomasz Boiński

## Opis projektu

Baza wizytówek internetowych dla prawników

#### Funkcjonalności:
* Baza użytkowników
* Zakładanie wizytówek
* Wyszukiwanie wizytówek wg kryteriów specjalizacji i miejsca świadczenia usług
* Przeglądanie wyszukanych wizytówek

#### Technologie:
* PHP
* system szablonów TWIG
* HTML
* CSS
* bootstrap
* javascript
* MySQL

#### Szczegóły techniczne:
* PHP obiektowe
* Komunikacja z MySQL za pomocą PDO  
* validator.js - do walidacji formularzy
* MVC

#### Co zaimplementowałem:
* Zapis, odczyt, modyfikacja danych w bazie MySQL
* Wyszukiwanie danych z bazy
* Wyszukiwarka odporna na reload i cofanie do poprzedniej strony
* mechanizm sesji
* kontorla dostępu do stron (na poziomie interface i walidacji zapytań HTTP)
* walidacja danych w formularzach po stronie wyszukiwarki
* wyświetlanie stron statycznych ('O nas', 'Polityka prywatności' itd.), poza kontrolerami

#### Czego nie zdążyłem:
* walidacji danych z formularzy po stronie serwera

### Instalacja
Repozytorium jest klonem mojego projektu netbeans.

## Zależności
* bootstrap
* validator.js
* TWIG
* PDO

### Projekt napisany przez:

Mirosław Sitek

sitek.miroslaw at gmail.com