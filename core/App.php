<?php

namespace core;

class App {

    const REDIRECT_PREFIX = 'redirect:';

    private $routing = [];
    private $baseDir;
    private $twig;
    public $logged_user;

    public function __construct($baseDir) {
        $this->baseDir = $baseDir;

        $loader = new \Twig_Loader_Filesystem($baseDir . '/../views');
        $this->twig = new \Twig_Environment($loader);
    }

    public function route($url, $controllerClass, $actionMethod) {
        $this->routing[$url] = [
            'class' => $controllerClass,
            'method' => $actionMethod
        ];
    }

    public function run() {
        //aspekty globalne
        session_start();

        //wybór kontrolera do wywołania:
        $action_url = $_GET['action']; // parametr zapytania get
        //echo '<br> parametr zapytania get - '.($action_url);  //debug
        $this->dispatch($action_url);
    }

    function dispatch($action_url) {

        if (!empty($this->routing[$action_url])) {

            $action = $this->routing[$action_url]; // pozycja z routingu
            $controller_class = "\\controllers\\${action['class']}"; // nazwa pliku z klasą routing=>class z katalogu controllers (teraz jest jeden kontroler)
            //obsługa stron statycznych
            if ($action['class'] == 'static') {
                $view_name = 'static/' . $action['method'];
                $model = [];
            } else {

                $controller = new $controller_class; //nowa instancja klasy z pliku o tej samej nazwie

                $model = [];
                $view_name = $controller->{$action['method']}($model); //za pomocą metody routing=>method klasy $controller_class budujemy $model
            }
        } else {
            $view_name = 'static/no_page';
            $model = [];
        }
        $this->build_response($view_name, $model); //zbuduj odpowiedź
    }

    function build_response($view, $model) {
        if (strpos($view, self::REDIRECT_PREFIX) === 0) {
            $url = substr($view, strlen(self::REDIRECT_PREFIX));
            header("Location: " . $url);
            exit;
        } else {
            $this->render($view, $model);
        }
    }

    function render($view_name, $model) {
        global $app;
        $model['sesja'] = $_SESSION;
        echo $this->twig->render($view_name . '.php.twig', $model);
    }

}
